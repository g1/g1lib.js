import mkdirp from 'mkdirp';

mkdirp.sync('generated/vendors');
mkdirp.sync('generated/tmpBrowser/context-dependant');
mkdirp.sync('generated/tmpNodejs/context-dependant');
mkdirp.sync('generated/npm/browser');
mkdirp.sync('generated/npm/nodejs');
