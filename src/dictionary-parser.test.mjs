import test from 'ava';
import * as app from './dictionary-parser.mjs';

test('parse remove ref:: lines', t => {
	t.is(app.parse('ref::truc',false), '');
});
test('parse handle <ref>', t => {
	t.is(app.parse(`ref::truc\nref::bidule\n<ref> <ref>`,false), '(truc|bidule) (truc|bidule)');
	t.is(app.parse(`ref::(truc|bidule)\n<ref> <ref>`,false), '(truc|bidule) (truc|bidule)');
});
test('parse handle <ref> complex inclusion', t => {
	t.is(app.parse(`ref::a<ref2>\n<ref>\nref2::(b|c)`,false), 'a(b|c)');
});
test('parse detect infinite recursion with <ref> and throw', t => {
	t.throws(()=>app.parse(`ref::a<ref>\n<ref>`,false));
});
test('parse detect infinite recursion with nested <ref> and throw', t => {
	t.throws(()=>app.parse(`ref::a<ref2>\n<ref>\nref2::b<ref>`,false));
});

test('parse handle [ -_]', t => {
	t.is(app.parse('[ -_]',false), '( |-|_)');
});
test('parse handle [a-f]', t => {
	t.is(app.parse('[a-f]',false), '(a|b|c|d|e|f)');
	t.is(app.parse('[7-9]',false), '(7|8|9)');
	t.is(app.parse('[C-F]',false), '(C|D|E|F)');
});
test('parse handle [a-c-]', t => {
	t.is(app.parse('[a-c-]',false), '(a|b|c|-)');
});

test('parse handle {qty}', t => {
	t.is(app.parse('a{5}',false), 'aaaaa');
});
test('parse handle {min,max}', t => {
	t.is(app.parse('b{3,5}',false), 'bbb(|b)(|b)');
});
test('parse handle (string){qty}', t => {
	t.is(app.parse(`c'est (toto|tata){0,2}`,false), `c'est (|toto|tata)(|toto|tata)`);
});
test('parse handle nested (s|t(ri|ng)){qty}', t => {
	t.is(app.parse(`(s|t(ri|ng)){1,2}`,false), `(s|t(ri|ng))(|s|t(ri|ng))`);
});

test('parse handle plop:\\:', t => {
	t.is(app.parse('plop:\\:ici',false), 'plop:\\:ici');
	t.is(app.parse('plop\\::ici',false), 'plop\\::ici');
	t.is(app.parse('plop::ici',false), '');
});
test('parse handle [\\]*]', t => {
	t.is(app.parse('[\\]*]',false), '(\\]|*)');
});

test('parse handle =ref>', t => {
	t.is(app.parse(`ref::truc\nref::bidule\n=ref> =ref>`,false), '(truc truc|bidule bidule)');
	t.is(app.parse(`ref::(truc|bidule)\n=ref> =ref>`,false), '(truc truc|bidule bidule)');
});
test('parse handle =ref> without generating duplication', t => {
	t.is(app.parse(`ref::(truc|bidule)\n(=ref> =ref>|machin)`,false), '(truc truc|machin|bidule bidule)');
});
test('parse handle multiple =ref>', t => {
	t.is(
		app.parse(`=ref> =ref2> =ref> =ref2>\nref::(truc|bidule)\nref2::(machin|chose)`,false),
		'(truc machin truc machin|bidule machin bidule machin|truc chose truc chose|bidule chose bidule chose)');
});
test('parse handle multi-level =ref>', t => {
	t.is(app.parse(`=ref> =ref> =ref2>\nref::(truc|=ref2>)\nref2::(machin|chose)`,false),
		'(truc truc machin|machin machin machin|truc truc chose|chose chose chose)');
});
test('parse throw if unconverted =ref>', t => t.throws(()=>app.parse(`=ref>\nref::=ref>`,false)));
/*TODO: test('parse handle multi-level =ref> in all case', t => {
	t.is(app.parse(`=ref2> =ref> =ref>\nref::(truc|=ref2>)\nref2::(machin|chose)`,false),
		'(machin truc truc|chose truc truc|machin machin machin|chose chose chose)');
});*/

test('no @@ idSec password separator ? implicit add it', t => {
	t.is(app.parse('a'), 'a@@a');
});
test('@@ present, do not add it', t => {
	t.is(app.parse('a@@b'), 'a@@b');
});
test('no @@ on each line ? combine no @@ lines', t => {
	t.is(app.parse(`
a@@(b|c)
(d|e)
(f|g)
h@@(i|j)
`), '(a@@(b|c)|h@@(i|j)|(d|e|f|g)@@(d|e|f|g))');
});
test('add @@ on part without it', t => {
	t.is(app.parse(`(a@@b|c)`), '(a@@b|c@@c)');
});
test('add @@ on part without it complex case', t => {
	t.is(app.parse(`(a(b|(c|d)@@(e|f)|g@@h|i)|(j|(k@@(l|m)n|o)))p`),
		'((a((c|d)@@(e|f)|g@@h)|k@@(l|m)n)p|(a(b|i)|j|o)p@@(a(b|i)|j|o)p)');
});
test('throw if multiple @@ in the same sequence', t => t.throws(() => app.parse(`a@@(b|c)@@d`)));

test('add no accents variant', t => t.is(app.parse('Ǧ1ǦdiT ici',false,1), '(Ǧ1ǦdiT ici|G1GdiT ici)'));
test('add optional accents variants', t => t.is(app.parse('Ǧ1ǦdiT ici',false,2), '(Ǧ|G)1(Ǧ|G)diT ici'));
test('add lowercase variant', t => t.is(app.parse('Ǧ1ǦdiT ici',false,0,1), '(Ǧ1ǦdiT ici|ǧ1ǧdit ici)'));
test('add optional lowercase variant', t => t.is(app.parse('Ǧ1ǦdiT ici',false,0,2), '(Ǧ|ǧ)1(Ǧ|ǧ)di(T|t) ici'));
test('add uppercase variants', t => t.is(app.parse('Ǧ1ǦdiT ici',false,0,3), '(Ǧ1ǦdiT ici|Ǧ1ǦdiT Ici|Ǧ1ǧdit Ici|Ǧ1ǦDIT ICI)'));
//test.skip('add optional capitalized variants', t => t.is(app.parse('Ǧ1ǦdiT ici',false,0,4), '(Ǧ|ǧ)1(Ǧ|ǧ)di(T|t) (I|i)ci'));
test('add no leetSpeak variants', t => t.is(app.parse('Ǧ1ǦdiT ici',false,0,0,1), 'Ǧ(1|i|l|I|L|t|T)ǦdiT ici'));
test('add leetSpeak variants', t => t.is(app.parse('Ǧ1ǦdiT ici',false,0,0,2), '(Ǧ1ǦdiT ici|Ǧ1Ǧd1(7|1) 1c1)'));
test('add every leetSpeak variants', t => t.is(app.parse('Ǧ1ǦdiT ici',false,0,0,3), 'Ǧ(1|i|l|I|L|t|T)Ǧd(i|1|l|I|L|t|T)(T|7|t|y|Y|1|i|l|I|L) (i|1|l|I|L|t|T)c(i|1|l|I|L|t|T)'));
//test('add all variants', t => t.true(app.parse('Ǧ1ǦdiT ici',true,2,4,3).length>500));
