import test from 'ava';
import * as app from './dictionary-tree.mjs';

const buildTreeThenSerialize = str => app.serialize(app.buildTreeStruct(str));

test('simple string still simple string', t => t.is(buildTreeThenSerialize('abc'), 'abc'));
test('(a|b) alt still (a|b)', t => t.is(buildTreeThenSerialize('(a|b)'), '(a|b)'));
test('a)b throw', t => t.throws(() => app.buildTreeStruct('a)b')));
// Ok to be permissive test('(a throw',t=>t.throws(()=>buildTreeStruct('(a')));
// Ok to be permissive test('a|b throw',t=>t.throws(()=>buildTreeStruct('a|b')));
test('(|b) keep empty choice', t => t.is(buildTreeThenSerialize('(|b)'), '(|b)'));
test('(b|b) trivial dedup', t => t.is(buildTreeThenSerialize('(|b||b|)'), '(|b)'));
test('a(b|c) mix fix and alt', t => t.is(buildTreeThenSerialize('a(b|c)'), 'a(b|c)'));
test('a(b) flat merge when no alt', t => t.is(buildTreeThenSerialize('a(b)'), 'ab'));
test('(a(b|c)|(d|e)) flat merge when unneeded depth', t => t.is(buildTreeThenSerialize('(a(b|c)|(d|e))'), '(a(b|c)|d|e)'));
test('build complexe tree with (|) pattern', t => t.is(buildTreeThenSerialize('(a(|b@@c|d|)|(e|f)|g|h@@i)'), '(a(|b@@c|d)|e|f|g|h@@i)'));

test('serialize incorrect tree throw', t => t.throws(() => app.serialize({plop: ['a']})));

test('splitAround throw when to many split', t => t.throws(() => app.splitAround('@@',app.buildTreeStruct('z@@a(b|c@@d)'))));
test('splitAround throw with @@@@', t => t.throws(() => app.splitAround('@@',app.buildTreeStruct('@@@@'))));
test('splitAround return notMatching case', t => t.deepEqual(app.splitAround('@',app.buildTreeStruct('a(b|c)')),{notMatching:'a(b|c)'}));
test('splitAround return matching case', t => t.deepEqual(app.splitAround('@',app.buildTreeStruct('a@b')),{matching:'a@b'}));
test('splitAround return both matching case and not matching one', t => t.deepEqual(app.splitAround('@',app.buildTreeStruct('a@b|c')),{matching:'a@b',notMatching:'c'}));

test('mono altCount', t => t.is(app.altCount(app.buildTreeStruct('ipsum')), 1));
test('simple altCount', t => t.is(app.altCount(app.buildTreeStruct('(lore|ipsu)m')), 2));
test('multi altCount', t => t.is(app.altCount(app.buildTreeStruct('(a|b|c)(d|e|f)g(h|i|j|k)')), 36));
test('multi level tree altCount', t => t.is(app.altCount(app.buildTreeStruct('a(b(c|d)|e(f|g|h)ij(k|l)|@@m)')), 9));

const exampleTree = () => app.buildTreeStruct('a(b(c|d)|e(f|g(h|i)|j)kl(m|n(o|p)|q(r|s)|t)|(u|v)w)');
// console.log(JSON.stringify(exampleTree()));
// console.log(app.serialize(exampleTree()));
/* exampleTree detailed
a( 0-27
  b(c|d) 0 & 1
 |e( 2-25
    f 2-7
   |g(h|i) 8-13 &14-19
   |j 20-25
  )kl( 2-25
		  m 2 & 8 & 14 & 20
		 |n(o|p) 3-4 & 9-10 & 15-16 & 21-22
		 |q(r|s) 5-6 & 11-12 & 17-18 & 23-24
		 |t 7 & 13 & 19 & 25
	)
 |(u|v)w 26-27
)
 */
test('getAlternative 0', t => t.is(app.getAlternative(0, exampleTree()), 'abc'));
test('getAlternative 1', t => t.is(app.getAlternative(1, exampleTree()), 'abd'));
test('getAlternative 2', t => t.is(app.getAlternative(2, exampleTree()), 'aefklm'));
test('getAlternative 3', t => t.is(app.getAlternative(3, exampleTree()), 'aefklno'));
test('getAlternative 4', t => t.is(app.getAlternative(4, exampleTree()), 'aefklnp'));
test('getAlternative 5', t => t.is(app.getAlternative(5, exampleTree()), 'aefklqr'));
test('getAlternative 6', t => t.is(app.getAlternative(6, exampleTree()), 'aefklqs'));
test('getAlternative 7', t => t.is(app.getAlternative(7, exampleTree()), 'aefklt'));
test('getAlternative 8', t => t.is(app.getAlternative(8, exampleTree()), 'aeghklm'));
test('getAlternative 9', t => t.is(app.getAlternative(9, exampleTree()), 'aeghklno'));
test('getAlternative 14', t => t.is(app.getAlternative(14, exampleTree()), 'aegiklm'));
test('getAlternative 20', t => t.is(app.getAlternative(20, exampleTree()), 'aejklm'));
test('getAlternative 26', t => t.is(app.getAlternative(26, exampleTree()), 'auw'));
test('getAlternative 27', t => t.is(app.getAlternative(27, exampleTree()), 'avw'));
test('getAlternative 28 or more throw', t => t.throws(() => app.getAlternative(28, exampleTree())));

test('escaped special characters are reconverted with getAlternative but not with getRawAlternative', t => {
	const tree = app.buildTreeStruct('a\\(b(c|d)@@e');
	t.is(app.getAlternative(0,tree), 'a(bc@@e');
	t.is(app.getRawAlternative(0,tree), `a${String.fromCharCode(0x01)}bc@@e`);
	const treeWhereRawIsUseful = app.buildTreeStruct('a\\@\\@b(c|d)@@e');
	t.is(app.getAlternative(0,treeWhereRawIsUseful), 'a@@bc@@e');
	t.is(app.getRawAlternative(0,treeWhereRawIsUseful), `a${String.fromCharCode(0x0e)+String.fromCharCode(0x0e)}bc@@e`);
});
