import test from 'ava';
import * as app from './dictionary.mjs';

function sleep(ms){
	return new Promise((resolve)=>setTimeout(resolve,ms));
}
test('get dictionary length', t => {
	const dictionaryString = '(a|b|c)d(e|f|g)';
	const dico = new app.Dictionary(dictionaryString,{idSecPwd:false});
	t.is(dico.length, 9);
});
test('get dictionary iteration', t => {
	const dictionaryString = '(a|b|c)d(e|f|g)';
	const dico = new app.Dictionary(dictionaryString);
	t.is(dico.get(5), "ade@@bdg");
});
test('get iteration are tracked', t => {
	const dico = new app.Dictionary('(a|b|c)d(e|f|g)');
	t.is(dico.tried, 0);
	dico.get(1);
	dico.get(2);
	t.is(dico.tried, 2);
});
test('dryGet iteration are not tracked', t => {
	const dico = new app.Dictionary('(a|b|c)d(e|f|g)');
	t.is(dico.tried, 0);
	dico.dryGet(1);
	dico.dryGet(2);
	t.is(dico.tried, 0);
});
test('_\\@\\@_@@_@\\@_ can be ambiguous with get, dryGet or not with rawGet, rawDryGet, splitGet', t => {
	const dico = new app.Dictionary('(\\@\\@|_@@\\)@\\@)', {cache:false});
	t.is(dico.length, 2);
	t.is(dico.dryGet(0), '_@@)@@');
	t.is(dico.dryGet(1), '@@@@@@');
	t.is(dico.get(1), '@@@@@@');
	const escAro = String.fromCharCode(0x0e);
	t.is(dico.rawGet(0), `_@@${String.fromCharCode(0x02)}@${escAro}`);
	t.is(dico.rawDryGet(1), `${escAro+escAro}@@${escAro+escAro}`);
	t.is(dico.splitGet(0)[0], '_');
	t.is(dico.splitGet(0)[1], ')@@');
	t.is(dico.splitGet(0).idSec, '_');
	t.is(dico.splitGet(0).pwd, ')@@');
	t.is(dico.splitGet(0).pass, ')@@');
	t.is(dico.splitDryGet(0)[0], '_');
	t.is(dico.splitDryGet(0)[1], ')@@');
});

test('get is time tracked', async t => {
	const dico = new app.Dictionary('(a|b|c)d(e|f|g)');
	dico.get(1);
	await sleep(5);
	dico.get(2);
	t.true(dico.timeSpent()>=0.005);
});
test('estimateDuration && estimateRemaining', t => {
	const dico = new app.Dictionary('(a|b|c)d(e|f|g)');
	dico.get(1);
	dico.get(2);
	t.is(dico.estimateDuration(), 81);
	t.is(dico.estimateRemaining(), 79);
});
test('get duplicated found count (from dictionary)', t => {
	const dictionaryString = '(a|b|c)d(e|f|g)';
	const dico = new app.Dictionary(dictionaryString);
	dico.get(0);
	dico.get(1);
	t.is(dico.duplicatedCount(), 0);
});
test('get duplicated found in past iteration (from dictionary)', t => {
	const dictionaryString = '((a|b)cd|(a|b)c(d|e)|bc(d|e))';
	const dico = new app.Dictionary(dictionaryString, {idSecPwd:false});
	for(let i = 0; i < dico.length;i++) dico.get(i);
	t.deepEqual(dico.duplicatedFound(), [ {alt:"bcd",index: [ 1, 4, 6 ]}, {alt:"acd",index: [ 0, 2 ]}, {alt:"bce",index: [ 5, 7 ]} ]);
});
test('dictionary can dryRun to find all duplicate', t => {
	const dictionaryString = '((a|b)cd|(a|b)c(d|e)|bc(d|e))';
	const dico = new app.Dictionary(dictionaryString, {idSecPwd:false});
	const duplicate = dico.dryRunDedup();
	t.deepEqual(duplicate, [ {alt:"bcd",index: [ 1, 4, 6 ]}, {alt:"acd",index: [ 0, 2 ]}, {alt:"bce",index: [ 5, 7 ]} ]);
	t.is(dico.length,8);
	t.is(dico.duplicateTotal,4);
});

test('dictionary can run with cache disabled', t => {
	const dictionaryString = '(a|b|c)d(e|f|g)';
	const dico = new app.Dictionary(dictionaryString,{cache:false});
	dico.get(0);
	t.is(Object.keys(dico.cache).length, 0);
});
test('dictionary can run with cache on', t => {
	const dictionaryString = 'a';
	const dico = new app.Dictionary(dictionaryString,{cache:true});
	dico.get(0);
	dico.get(0);
	t.is(dico.duplicatedCount(), 1);
});
test('duplicate match §duplicate§ pattern', t => {
	const dictionaryString = 'a';
	const dico = new app.Dictionary(dictionaryString);
	const first = dico.get(0);
	const second = dico.get(0);
	t.is(first, 'a@@a');
	t.is(second, '§duplicate§a@@a');
});
test('dictionary called with speed option try to activate variante accent, caps and leetSpeak option to reach 1h of compute estimated time', t => {
	const dictionaryString = 'Ǧ1Ǧ1';
	const dico = new app.Dictionary(dictionaryString, {speed:30});
	t.is(dico.length,272);
	t.is(dico.duplicateTotal,16);
	t.is(dico.duplicateRatio.toPrecision(3),'0.0588');
	t.is(dico.uniqueRatio.toPrecision(3),'0.941');
});
test('dico should not crash', t=>{
	const v8CrashStringButFirefoxWork = '(((Ǧ|ǧ)|(G|g))(1|i|l|I)((Ǧ|ǧ)|(G|g))(1|i|l|I)@@((Ǧ|ǧ)|(G|g))(1|i|l|I)((Ǧ|ǧ)|(G|g))(1|i|l|I)|(((Ǧ|ǧ)|(G|g))(1|i|l|I)((Ǧ|ǧ)|(G|g))(1|i|l|I)@@((Ǧ|ǧ)|(G|g))(1|i|l|I)((Ǧ|ǧ)|(G|g))(1|i|l|I)|(ǧ|g)(1|i|l|I)(ǧ|g)(1|i|l|I)@@(ǧ|g)(1|i|l|I)(ǧ|g)(1|i|l|I)))';
	const dico = new app.Dictionary(v8CrashStringButFirefoxWork,{speed:30})
	t.is(dico.length,70928);
	t.is(dico.duplicateTotal,5392);
	t.is(dico.duplicateRatio.toFixed(3),'0.076');
	t.is(dico.uniqueRatio.toFixed(3),'0.924');
	t.is(dico.estimateDuration().toFixed(0),'2364');
});
test('just under 1h : try to find variant', t => {
	const dictionaryString = 'onlyOne@@[a-z0-8]';
	const dico = new app.Dictionary(dictionaryString, {speed:0.01});
	t.is(dico.length,35);
	t.is(dico.duplicateTotal,0);
});
test("just over 1h : don't search for variants", t => {
	const dictionaryString = 'onlyOne@@[a-z0-9 ]';
	const dico = new app.Dictionary(dictionaryString, {speed:0.01});
	t.is(dico.length,37);
	t.is(typeof dico.duplicateTotal, 'undefined');
});
test("alt number >= 1_000_000 disable cache and variants attempt", t => {
	const dico = new app.Dictionary(`[0-9]{6}@@onlyOne`, {speed:1_000_000});
	t.is(dico.length,1_000_000);
	t.is(typeof dico.duplicateTotal, 'undefined');
});
test("huge alt number work fine", t => {
	const x = 12;
	const dico = new app.Dictionary(`[0-9]{${2+x}}@@[a-z0-9 ]`, {speed: 10**x});
	t.is(dico.length,3_700 * Math.pow(10,x));
	t.is(typeof dico.duplicateTotal, 'undefined');
});
test("huge alt number apply variants if set", t => {
	const x = 12;
	t.is((new app.Dictionary(`[0-9]{${2+x}}@@[A-Z]`, {lowerCase:0})).length,2_600 * 10**x);
	t.is((new app.Dictionary(`[0-9]{${2+x}}@@[A-Z]`, {lowerCase:1})).length,5_200 * 10**x);
});
test('escaped special characters still escaped when re-serialized', t => {
	const dictionaryString = '(a|b|c\\)c)d(e|f|g)';
	const dico = new app.Dictionary(dictionaryString, {idSecPwd:false});
	const serialized = dico.serialize();
	t.is(serialized, dictionaryString);
	t.is(dico.get(6), 'c)cde');
});
