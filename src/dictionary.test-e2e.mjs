import test from 'ava';
import * as app from './dictionary.mjs';

test("alt < 1_000_000 & estimateDuration < 1h search for variant and use cache to fine duplicate", t => {
	const dico = new app.Dictionary(`[0-9]{4}@@[a-z0-8][01]`, {speed:200});
	t.is(dico.length,700_000);
	t.is(dico.duplicateTotal,0);
});
