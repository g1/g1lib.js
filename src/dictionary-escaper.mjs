const specialMap = {
	'(': String.fromCharCode(0x01),
	')': String.fromCharCode(0x02),
	'|': String.fromCharCode(0x03),
	'{': String.fromCharCode(0x04),
	'}': String.fromCharCode(0x05),
	',': String.fromCharCode(0x06),
	'[': String.fromCharCode(0x07),
	']': String.fromCharCode(0x08),
	'-': String.fromCharCode(0x09),
	'<': String.fromCharCode(0x0a),
	'>': String.fromCharCode(0x0b),
	':': String.fromCharCode(0x0c),
	'=': String.fromCharCode(0x0d),
	'@': String.fromCharCode(0x0e)
};
const revertSpecial = swapKeyValue(specialMap);
function swapKeyValue(object) {
	const result = {};
	for (const key in object) {
		result[object[key]] = key;
	}

	return result;
}
export function escape2utfSpecial(str) {
	return str.replace(/\\(.)/g, (a, chr) => specialMap[chr] ? specialMap[chr] : chr);
}
export function utfSpecial2unEscaped(str) {
	return str.split('').map(chr => revertSpecial[chr] ? revertSpecial[chr] : chr).join('');
}
export function utfSpecial2escaped(str) {
	return str.split('').map(chr => revertSpecial[chr] ? `\\${revertSpecial[chr]}` : chr).join('');
}
